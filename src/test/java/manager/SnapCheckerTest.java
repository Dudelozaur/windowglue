package manager;

import com.google.common.collect.Lists;
import manager.shadow.Intersection;
import manager.shadow.IntersectionPosition;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class SnapCheckerTest {

    public static final int SNAP_DISTANCE = 20;

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @Test
    public void testGetAllIntersections() throws Exception {
        SnapChecker checker = new SnapChecker(SNAP_DISTANCE);
        JFrame w1 = new JFrame("W1");
        JFrame w2 = new JFrame("W2");
        JFrame w3 = new JFrame("W3");
        w1.setBounds(0, 0, 100, 100);
        w2.setBounds(105, 0, 100, 100);
        w3.setBounds(210, 0, 100, 100);
        checker.maintainShadow(w1);
        checker.maintainShadow(w2);
        checker.maintainShadow(w3);
        final List<Intersection> allIntersections = Lists.newArrayList(checker.getAllIntersections(w2));
        assertEquals(2, allIntersections.size());
        assertEquals(IntersectionPosition.RIGHT, allIntersections.get(1).getIntersectionPosition());
        assertEquals(w3, allIntersections.get(1).getWindow());
        assertEquals(IntersectionPosition.LEFT, allIntersections.get(0).getIntersectionPosition());
        assertEquals(w1, allIntersections.get(0).getWindow());
    }

    @Test
    public void testMaintainShadow() throws Exception {
        JFrame w1 = new JFrame("W1");
        w1.setBounds(10, 10, 100, 100);

        SnapChecker checker = new SnapChecker(SNAP_DISTANCE);
        checker.maintainShadow(w1);
        assertEquals(new Rectangle(25, 10, 85, SNAP_DISTANCE), checker.getShadowArea().get(w1).getTop());
        assertEquals(new Rectangle(25, 110, 85, SNAP_DISTANCE), checker.getShadowArea().get(w1).getBottom());
        assertEquals(new Rectangle(10, 25, SNAP_DISTANCE, 85), checker.getShadowArea().get(w1).getLeft());
        assertEquals(new Rectangle(110, 25, SNAP_DISTANCE, 85), checker.getShadowArea().get(w1).getRight());
    }
}