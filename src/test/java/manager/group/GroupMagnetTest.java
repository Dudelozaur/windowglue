package manager.group;

import com.beust.jcommander.internal.Sets;
import manager.shadow.Intersection;
import manager.shadow.IntersectionPosition;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class GroupMagnetTest {

    @Test
    public void testMoveWithBoth() {
        Window w1 = new JFrame("w1");
        Window w2 = new JFrame("w2");
        Window w3 = new JFrame("w3");
        Set<Intersection> intersections = Sets.newHashSet();
        intersections.add(new Intersection(IntersectionPosition.LEFT, w2));
        intersections.add(new Intersection(IntersectionPosition.TOP, w3));

        w1.setBounds(0, 0, 100, 100);
        w2.setBounds(200, 300, 100, 100);
        w3.setBounds(250, 100, 100, 100);

        assertEquals(GroupMagnet.moveWindowWithBoth(w1, intersections), new Point(300, 200));
    }
}