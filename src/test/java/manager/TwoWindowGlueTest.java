package manager;

import manager.listeners.ListenerType;
import org.fest.swing.core.BasicRobot;
import org.fest.swing.core.EdtSafeCondition;
import org.fest.swing.core.Robot;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.timing.Pause;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

import static org.testng.Assert.assertEquals;

/**
 * @author vlad.topala
 */
public class TwoWindowGlueTest {

    @Test
    public void TestTwoGlue() {

        final GlueManager m = new GlueManager(20, KeyEvent.CTRL_MASK, ListenerType.NATIVE);
        m.forceGlueMode(true);
        Robot r = BasicRobot.robotWithCurrentAwtHierarchy();

        JFrame w0 = new JFrame("Window" + 0);
        w0.setName("w" + 0);
        JButton b = new JButton("B" + 0);
        w0.add(b);
        w0.setVisible(true);
        w0.setSize(200, 200);
        w0.setLocation(0, 0);

        final JFrame w1 = new JFrame("Window1");
        w1.setName("w1");
        JButton b1 = new JButton("B1");
        w1.add(b1);
        w1.setVisible(true);
        w1.setSize(200, 200);
        w1.setLocation(500, 0);

        FrameFixture f0 = new FrameFixture(r, w0);
        FrameFixture f1 = new FrameFixture(r, w1);

        f1.moveTo(new Point(200, 0));
        m.forceGlueMode(false);

        f0.moveTo(new Point(400, 200));

        Pause.pause(new EdtSafeCondition("Windows moved together") {
            @Override
            protected boolean testInEDT() {
                return new Point(600, 200).equals(w1.getLocation());
            }
        }, 1000);
        assertEquals(new Point(600, 200), w1.getLocation());
        assertEquals(new Point(400, 200), w0.getLocation());
    }
}
