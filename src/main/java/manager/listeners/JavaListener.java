package manager.listeners;

import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.InputEvent;

/**
 * @author vlad.topala
 */
public class JavaListener extends GlueListener {

    private final long eventMask;
    private final AWTEventListener listener = new AWTEventListener() {
        @Override
        public void eventDispatched(AWTEvent e) {
            InputEvent inputEvent = (InputEvent) e;
            setSnapModeOn((inputEvent.getModifiersEx() & snapMask) == snapMask);
        }
    };

    public JavaListener(int snapMask, SnapModeChangeCallback callback) {
        super(snapMask, callback);
        eventMask = AWTEvent.KEY_EVENT_MASK + AWTEvent.MOUSE_EVENT_MASK + AWTEvent.MOUSE_MOTION_EVENT_MASK;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            Toolkit.getDefaultToolkit().addAWTEventListener(listener, eventMask);
        } else {
            Toolkit.getDefaultToolkit().removeAWTEventListener(listener);
        }
    }
}
