package manager.listeners;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * @author vlad.topala
 */
public class NativeListener extends GlueListener implements NativeKeyListener, NativeMouseListener {

    public NativeListener(int snapMask, SnapModeChangeCallback callback) {
        super(snapMask, callback);
        initListener();
    }

    private void initListener() {
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        LogManager.getLogManager().reset();
        logger.setLevel(Level.OFF);
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());
            ex.printStackTrace();

            System.exit(1);
        }

        //Construct the example object and initialze native hook.
        GlobalScreen.getInstance().addNativeMouseListener(this);
        GlobalScreen.getInstance().addNativeKeyListener(this);
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        resetSnapMode(nativeKeyEvent);
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        resetSnapMode(nativeKeyEvent);
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        resetSnapMode(nativeKeyEvent);
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {
        resetSnapMode(nativeMouseEvent);
    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
        resetSnapMode(nativeMouseEvent);
    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {
        resetSnapMode(nativeMouseEvent);
    }

    private void resetSnapMode(NativeInputEvent e) {
        setSnapModeOn((e.getModifiers() & snapMask) == snapMask);
    }
}
