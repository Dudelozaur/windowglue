package manager.listeners;

/**
 * @author vlad.topala
 */
public class GlueListenerFactory {

    public static GlueListener getListener(ListenerType type, int mask, SnapModeChangeCallback callback) {
        switch (type) {
            case NATIVE:
                return new NativeListener(mask, callback);
            default:
                return new JavaListener(mask, callback);
        }
    }
}
