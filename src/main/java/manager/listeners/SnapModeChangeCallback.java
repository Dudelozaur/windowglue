package manager.listeners;

/**
 * @author vlad.topala
 */
public interface SnapModeChangeCallback {

    public void onSnapModeChange(boolean newValue);

}
