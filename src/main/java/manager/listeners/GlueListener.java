package manager.listeners;

/**
 * @author vlad.topala
 */
public abstract class GlueListener {

    /**
     * The keyboard and mouse modifiers that have to be present when moving a
     * window such that it will be in snap mode
     * Ex: MouseEvent.BUTTON1_DOWN_MASK | MouseEvent.BUTTON3_DOWN_MASK
     * This requires both left and right mouse buttons to be pressed for snap
     * mode
     */
    protected final int snapMask;

    protected final SnapModeChangeCallback callback;

    /**
     * When certain modifiers are pressed the glueMode is on
     */
    protected boolean snapModeOn;

    /**
     * Should listen for snap triggers or not
     */
    protected boolean enabled;

    public GlueListener(int snapMask, SnapModeChangeCallback callback) {
        this.snapMask = snapMask;
        this.callback = callback;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSnapModeOn() {
        return enabled && snapModeOn;
    }

    protected void setSnapModeOn(boolean on) {
        snapModeOn = on;
        callback.onSnapModeChange(snapModeOn);
    }

}
