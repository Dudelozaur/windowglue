package manager.listeners;

/**
 * @author vlad.topala
 */
public enum ListenerType {
    NATIVE, JAVA
}
