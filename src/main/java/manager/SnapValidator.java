package manager;

import manager.shadow.Intersection;

import java.util.Set;

/**
 * @author vlad.topala
 */
public class SnapValidator {

    /**
     * This method filters out the intersections that are contradictory.
     * For example if a window is between two other windows and could glue to any of them
     * but not both (the distance between those two is not equal to the width of the
     * moved window)
     *
     * @param intersections - all intersections
     * @return - intersections with windows that can form a glued group
     */
    public Set<Intersection> getBestIntersections(Set<Intersection> intersections) {
        return intersections;
    }

}
