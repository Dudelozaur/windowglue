package manager;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import manager.shadow.Intersection;
import manager.shadow.IntersectionPosition;
import manager.shadow.WindowShadow;

import java.awt.*;
import java.util.Map;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class SnapChecker {

    public static final int CORNER_DISTANCE = 15;
    /**
     * Hold invisible borders for all windows N, E, S, W such that
     * checking intersections will be easy
     */
    private final Map<Window, WindowShadow> shadowArea = Maps.newHashMap();

    /**
     * The distance in pixels between two windows before the moving one is
     * moved next to the other
     */
    private final int snapDistance;

    public SnapChecker(int snapDistance) {
        this.snapDistance = snapDistance;
    }

    public Set<Intersection> getAllIntersections(Window w) {

        WindowShadow sourceShadow = shadowArea.remove(w);
        Set<Intersection> intersections = Sets.newHashSet();

        for (Map.Entry<Window, WindowShadow> windowRectangleEntry : shadowArea.entrySet()) {

            IntersectionPosition p = checkShadowIntersection(sourceShadow, windowRectangleEntry.getValue());
            if (p != IntersectionPosition.NO_INTERSECTION) {
                intersections.add(new Intersection(p, windowRectangleEntry.getKey()));
            }
        }

        shadowArea.put(w, sourceShadow);

        return intersections;

    }

    //todo use snapDistance / 2
    public void maintainShadow(Window source) {

        Rectangle top = new Rectangle(
                source.getX() + CORNER_DISTANCE,
                source.getY(),
                source.getWidth() - CORNER_DISTANCE,
                snapDistance);
        Rectangle bottom = new Rectangle(
                source.getX() + CORNER_DISTANCE,
                source.getY() + source.getHeight(),
                source.getWidth() - CORNER_DISTANCE,
                snapDistance);
        Rectangle right = new Rectangle(
                source.getX() + source.getWidth(),
                source.getY() + CORNER_DISTANCE,
                snapDistance,
                source.getHeight() - CORNER_DISTANCE);
        Rectangle left = new Rectangle(
                source.getX(),
                source.getY() + CORNER_DISTANCE,
                snapDistance,
                source.getHeight() - CORNER_DISTANCE);

        shadowArea.put(source, new WindowShadow(top, left, bottom, right));

    }

    private IntersectionPosition checkShadowIntersection(WindowShadow sourceShadow, WindowShadow secondary) {

        return sourceShadow.getRight().intersects(secondary.getLeft())
                ? IntersectionPosition.RIGHT
                : sourceShadow.getBottom().intersects(secondary.getTop())
                ? IntersectionPosition.BOTTOM
                : sourceShadow.getLeft().intersects(secondary.getRight())
                ? IntersectionPosition.LEFT
                : sourceShadow.getTop().intersects(secondary.getBottom())
                ? IntersectionPosition.TOP
                : IntersectionPosition.NO_INTERSECTION;

    }

}
