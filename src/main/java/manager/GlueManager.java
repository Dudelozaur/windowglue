package manager;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import decoration.DecorationManager;
import decoration.WindowDecorator;
import lombok.NonNull;
import manager.group.GroupsManager;
import manager.listeners.GlueListener;
import manager.listeners.GlueListenerFactory;
import manager.listeners.ListenerType;
import manager.listeners.SnapModeChangeCallback;
import manager.shadow.Intersection;
import matching.Matcher;

import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class GlueManager {

    private static final int DEFAULT_SNAP_DISTANCE = 20;
    private static final int DEFAULT_SNAP_MASK = KeyEvent.CTRL_MASK;

    private final GroupsManager groupsManager;
    private final SnapChecker snapChecker;
    private final SnapValidator snapValidator;
    private final List<Matcher> matchers = Lists.newArrayList();
    private GlueListener glueListener;
    private boolean forceGlueMode;
    private DecorationManager decorationManager;


    public GlueManager() {
        this(ListenerType.NATIVE);
    }

    public GlueManager(ListenerType type) {
        this(DEFAULT_SNAP_DISTANCE, DEFAULT_SNAP_MASK, type);
    }

    /**
     * @param snapDistance - The distance in pixels between two windows before the moving one is
     *                     moved next to the other
     * @param snapMask     - the snap trigger - key or mouse button that has to be pressed while
     *                     moving the window to be in glue mode
     * @param listenerType - type of listener depending on window decorations
     */
    public GlueManager(int snapDistance, int snapMask, ListenerType listenerType) {

        groupsManager = new GroupsManager();
        snapChecker = new SnapChecker(snapDistance);
        snapValidator = new SnapValidator();
        decorationManager = new DecorationManager();

        glueListener = GlueListenerFactory.getListener(listenerType, snapMask,
                new SnapModeChangeCallback() {
                    @Override
                    public void onSnapModeChange(boolean snapModeOn) {
                        groupsManager.setMovementOn(!snapModeOn);
                        if (!snapModeOn) {
                            decorationManager.clearDecorations();
                        }
                    }
                }
        );
        initAwtEventListener();
        glueListener.setEnabled(true);
    }

    @VisibleForTesting
    protected void forceGlueMode(boolean force) {
        forceGlueMode = force;
    }

    private void initAwtEventListener() {
        final long eventMask = AWTEvent.COMPONENT_EVENT_MASK + AWTEvent.CONTAINER_EVENT_MASK
                + AWTEvent.WINDOW_EVENT_MASK + AWTEvent.WINDOW_FOCUS_EVENT_MASK
                + AWTEvent.WINDOW_STATE_EVENT_MASK;

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
            @Override
            public void eventDispatched(AWTEvent e) {
                if (!(e.getSource() instanceof Window)) {
                    return;
                }

                Window source = (Window) e.getSource();
                if (!matches(source)) {
                    return;
                }

                switch (e.getID()) {
                    case ComponentEvent.COMPONENT_MOVED:
                    case ComponentEvent.COMPONENT_RESIZED:
                        snapChecker.maintainShadow(source);
                        checkIntersection(source);
                        break;
                }
            }
        }, eventMask);
    }

    private void checkIntersection(Window source) {

        if (!glueListener.isSnapModeOn() && !forceGlueMode) {
            return;
        }

        Set<Intersection> intersections = snapChecker.getAllIntersections(source);
        Set<Intersection> possibleIntersections = snapValidator.getBestIntersections(intersections);

        if (!possibleIntersections.isEmpty()) {
            decorationManager.decorateWindows(possibleIntersections);
            groupsManager.groupWindows(source, possibleIntersections);
        } else {
            decorationManager.clearDecorations();
            groupsManager.removeWindowFromGroup(source);
        }
    }

    private boolean matches(Window source) {
        for (Matcher matcher : matchers) {
            if (!matcher.matches(source)) {
                return false;
            }
        }
        return true;
    }

    public void addWindowMatcher(@NonNull Matcher m) {
        matchers.add(m);
    }

    public void removeWindowMatcher(@NonNull Matcher m) {
        matchers.remove(m);
    }

    public void addWindowDecorator(@NonNull WindowDecorator decorator) {
        decorationManager.addWindowDecorator(decorator);
    }

    public void removeWindowDecorator(@NonNull WindowDecorator decorator) {
        decorationManager.removeWindowDecorator(decorator);
    }
}
