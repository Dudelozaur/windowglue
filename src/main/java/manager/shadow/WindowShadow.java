package manager.shadow;

import lombok.Getter;

import java.awt.*;

/**
 * @author vlad.topala
 */
public class WindowShadow {

    @Getter
    private final Rectangle top;
    @Getter
    private final Rectangle left;
    @Getter
    private final Rectangle bottom;
    @Getter
    private final Rectangle right;

    public WindowShadow(Rectangle top, Rectangle left, Rectangle bottom, Rectangle right) {

        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;

    }
}
