package manager.shadow;

/**
 * @author vlad.topala
 */
public enum IntersectionAxis {
    NONE, VERTICAL, HORIZONTAL, BOTH;

    public static IntersectionAxis getAxisFromPosition(IntersectionPosition position) {
        switch (position) {
            case TOP:
            case BOTTOM:
                return VERTICAL;
            case LEFT:
            case RIGHT:
                return HORIZONTAL;
            default:
                return NONE;
        }
    }

    public static boolean areOpposite(IntersectionAxis axis, IntersectionAxis axis1) {
        return axis == VERTICAL && axis1 == HORIZONTAL
                || axis == HORIZONTAL && axis1 == VERTICAL;
    }
}
