package manager.shadow;

/**
 * @author vlad.topala
 */
public enum IntersectionPosition {
    TOP, BOTTOM, LEFT, RIGHT, NO_INTERSECTION
}
