package manager.shadow;

import lombok.Getter;

import java.awt.*;

/**
 * @author vlad.topala
 */
public class Intersection {

    @Getter
    private final IntersectionPosition intersectionPosition;

    @Getter
    private final Window window;

    public Intersection(IntersectionPosition intersectionPosition, Window window) {
        this.intersectionPosition = intersectionPosition;
        this.window = window;
    }

}
