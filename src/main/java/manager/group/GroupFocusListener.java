package manager.group;

import com.google.common.collect.Sets;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class GroupFocusListener extends WindowAdapter {
    private final Collection<Window> windows;

    public GroupFocusListener(Collection<Window> windows) {
        this.windows = Sets.newHashSet();
        for (Window window : windows) {
            addWindow(window);
        }
    }

    protected void addWindow(Window window) {
        windows.add(window);
        window.addWindowFocusListener(this);
    }

    protected void removeWindow(Window window) {
        windows.remove(window);
        window.removeWindowFocusListener(this);
    }

    protected void removeWindows(Set<Window> windows) {

    }

    protected void cleanUp() {

    }

    @Override
    public void windowGainedFocus(final WindowEvent e) {
        System.out.println(e.getOppositeWindow() == null ? "null" : e.getOppositeWindow().getName());
        if (!windows.contains(e.getOppositeWindow())) {
            for (Window window : windows) {
                window.toFront();
            }
            e.getWindow().requestFocus();
        }
    }
}
