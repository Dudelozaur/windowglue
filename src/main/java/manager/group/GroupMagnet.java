package manager.group;

import manager.shadow.Intersection;
import manager.shadow.IntersectionAxis;
import manager.shadow.IntersectionPosition;

import java.awt.*;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class GroupMagnet {

    private static final int CORNER_SNAP_DISTANCE = 15;

    /**
     * When in snap range the window will be moved to position
     *
     * @param source        - the window that was moved by the user
     * @param intersections - all the windows the moved window intersects
     */
    protected static void moveWindowToGroup(Window source, Set<Intersection> intersections) {
        IntersectionAxis axis = getIntersectionAxes(intersections);
        Point newLocation = new Point();
        switch (axis) {
            case BOTH:
                newLocation = moveWindowWithBoth(source, intersections);
                break;
            case HORIZONTAL:
                newLocation = moveWindowWithHorizontal(source, intersections);
                break;
            case VERTICAL:
                newLocation = moveWindowWithVertical(source, intersections);
                break;
        }
        source.setLocation(newLocation);
    }

    private static Point moveWindowWithVertical(Window source, Set<Intersection> intersections) {
        Intersection i = intersections.iterator().next();
        int y = i.getIntersectionPosition() == IntersectionPosition.TOP
                ? i.getWindow().getY() + i.getWindow().getHeight()
                : i.getWindow().getY() - source.getHeight();
        int finalX = snapCoordinateToCorner(source.getX(), intersections, IntersectionAxis.VERTICAL);
        return new Point(finalX, y);
    }

    private static Point moveWindowWithHorizontal(Window source, Set<Intersection> intersections) {
        Intersection i = intersections.iterator().next();
        int x = i.getIntersectionPosition() == IntersectionPosition.LEFT
                ? i.getWindow().getX() + i.getWindow().getWidth()
                : i.getWindow().getX() - source.getWidth();
        int finalY = snapCoordinateToCorner(source.getY(), intersections, IntersectionAxis.HORIZONTAL);
        return new Point(x, finalY);

    }

    private static int snapCoordinateToCorner(int sourceCoord, Set<Intersection> intersections, IntersectionAxis axis) {
        int finalCoord = sourceCoord;
        int minDist = CORNER_SNAP_DISTANCE;
        for (Intersection intersection : intersections) {
            if (axis == IntersectionAxis.VERTICAL) {
                if (Math.abs(intersection.getWindow().getX() - sourceCoord) < minDist) {
                    finalCoord = intersection.getWindow().getX();
                    minDist = Math.abs(intersection.getWindow().getX() - sourceCoord);
                }
            } else {
                if (Math.abs(intersection.getWindow().getY() - sourceCoord) < minDist) {
                    finalCoord = intersection.getWindow().getY();
                    minDist = Math.abs(intersection.getWindow().getY() - sourceCoord);
                }
            }
        }
        return finalCoord;
    }

    private static Point moveWindowWithBoth(Window source, Set<Intersection> intersections) {
        Intersection horizontal = null;
        Intersection vertical = null;

        for (Intersection intersection : intersections) {
            if (horizontal != null && vertical != null) {
                break;
            }
            if (IntersectionAxis.getAxisFromPosition(intersection.getIntersectionPosition())
                    == IntersectionAxis.HORIZONTAL) {
                horizontal = intersection;
            } else {
                vertical = intersection;
            }
        }

        int x, y;

        Window horizontalWindow = horizontal.getWindow();
        Window verticalWindow = vertical.getWindow();
        if (horizontal.getIntersectionPosition() == IntersectionPosition.LEFT) {
            x = horizontalWindow.getX() + horizontalWindow.getWidth();
        } else {
            x = horizontalWindow.getX() - source.getWidth();
        }

        if (vertical.getIntersectionPosition() == IntersectionPosition.TOP) {
            y = verticalWindow.getY() + verticalWindow.getHeight();
        } else {
            y = verticalWindow.getY() - source.getHeight();
        }
        return new Point(x, y);
    }

    private static IntersectionAxis getIntersectionAxes(Set<Intersection> intersections) {

        IntersectionAxis axis = IntersectionAxis.NONE;

        for (Intersection intersection : intersections) {
            IntersectionAxis axis1 = IntersectionAxis.getAxisFromPosition(intersection.getIntersectionPosition());
            if (IntersectionAxis.areOpposite(axis, axis1)) {
                return IntersectionAxis.BOTH;
            }
            axis = axis1;
        }

        return axis;
    }
}
