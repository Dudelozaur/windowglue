package manager.group;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import manager.group.handlers.EventHandler;
import manager.group.handlers.movement.MovementHandler;
import manager.group.handlers.resize.RemoveOnResizeHandler;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Collection;
import java.util.Map;

/**
 * @author vlad.topala
 */
public class GroupComponentListener extends ComponentAdapter {

    private final Map<Component, Rectangle> previousBounds = Maps.newHashMap();
    private final EventHandler resizeHandler;
    private final EventHandler movementHandler;
    private final Collection<Window> windows;
    private final RemoveWindowCallback removeWindowCallback;
    private boolean movementOn;

    public GroupComponentListener(Collection<Window> windows, RemoveWindowCallback removeWindowCallback) {
        this.removeWindowCallback = removeWindowCallback;
        this.windows = Sets.newHashSet();
        for (Window window : windows) {
            addWindow(window);
        }
        resizeHandler = new RemoveOnResizeHandler();
        movementHandler = new MovementHandler();
    }

    public void addWindow(Window w) {
        windows.add(w);
        w.addComponentListener(this);
        previousBounds.put(w, w.getBounds());
    }

    public void removeWindows(Iterable<Window> windows) {
        for (Window w : windows) {
            removeWindow(w);
        }
    }

    public void cleanUp() {
        for (Window w : windows) {
            w.removeComponentListener(this);
        }
        windows.clear();
        previousBounds.clear();
    }

    public void removeWindow(Window w) {
        w.removeComponentListener(this);
        previousBounds.remove(w);
        windows.remove(w);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        movementHandler.handleEvent(e, previousBounds, movementOn, windows, removeWindowCallback);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        resizeHandler.handleEvent(e, previousBounds, movementOn, windows, removeWindowCallback);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        removeWindowCallback.onRemoveWindow((Window) e.getComponent());
    }

    public void setMovementOn(boolean movementOn) {
        this.movementOn = movementOn;
    }
}
