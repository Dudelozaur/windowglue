package manager.group;

import java.awt.*;

/**
 * @author vlad.topala
 */
public interface RemoveWindowCallback {

    public void onRemoveWindow(Window w);

}
