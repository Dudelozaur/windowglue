package manager.group.handlers.resize;

import manager.group.RemoveWindowCallback;
import manager.group.handlers.EventHandler;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.util.Collection;
import java.util.Map;

/**
 * @author vlad.topala
 */
public class AccordionResizeHandler implements EventHandler {

    @Override
    public void handleEvent(ComponentEvent e, Map<Component, Rectangle> previousBounds,
                            boolean movementOn, Collection<Window> windows, RemoveWindowCallback callback) {

    }
}
