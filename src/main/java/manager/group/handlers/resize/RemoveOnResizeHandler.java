package manager.group.handlers.resize;

import manager.SnapChecker;
import manager.group.RemoveWindowCallback;
import manager.group.handlers.EventHandler;
import manager.shadow.Intersection;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class RemoveOnResizeHandler implements EventHandler {

    SnapChecker snapChecker = new SnapChecker(1);

    @Override
    public void handleEvent(ComponentEvent e, Map<Component, Rectangle> previousBounds,
                            boolean movementOn, Collection<Window> windows, RemoveWindowCallback callback) {

        Component firstWindow = e.getComponent();

        if (!windows.contains(firstWindow)) {
            return;
        }

        for (Window window : windows) {
            snapChecker.maintainShadow(window);
        }

        Set<Intersection> allIntersections = snapChecker.getAllIntersections((Window) firstWindow);

        if (allIntersections.isEmpty()) {
            callback.onRemoveWindow((Window) firstWindow);
        }
    }
}
