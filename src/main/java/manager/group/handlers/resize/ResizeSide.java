package manager.group.handlers.resize;

import manager.shadow.IntersectionPosition;

import java.util.EnumSet;

/**
 * @author by vlad.topala
 */
public enum ResizeSide {
    TOP, BOTTOM, LEFT, RIGHT, TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT;

    public EnumSet<IntersectionPosition> getResizeIntersectionPositions() {
        switch (this) {
            case TOP:
                return EnumSet.of(IntersectionPosition.TOP);
            case BOTTOM:
                return EnumSet.of(IntersectionPosition.BOTTOM);
            case LEFT:
                return EnumSet.of(IntersectionPosition.LEFT);
            case RIGHT:
                return EnumSet.of(IntersectionPosition.RIGHT);
            case TOP_LEFT:
                return EnumSet.of(IntersectionPosition.TOP, IntersectionPosition.LEFT);
            case TOP_RIGHT:
                return EnumSet.of(IntersectionPosition.TOP, IntersectionPosition.RIGHT);
            case BOTTOM_LEFT:
                return EnumSet.of(IntersectionPosition.TOP, IntersectionPosition.LEFT);
            case BOTTOM_RIGHT:
                return EnumSet.of(IntersectionPosition.TOP, IntersectionPosition.RIGHT);
            default:
                return null;
        }
    }
}
