package manager.group.handlers.movement;

import manager.group.RemoveWindowCallback;
import manager.group.handlers.EventHandler;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.util.Collection;
import java.util.Map;

/**
 * @author vlad.topala
 */
public class MovementHandler implements EventHandler {
    @Override
    public void handleEvent(ComponentEvent e, Map<Component, Rectangle> previousBounds,
                            boolean movementOn, Collection<Window> windows, RemoveWindowCallback callback) {

        Component firstWindow = e.getComponent();
        if (!movementOn) {
            previousBounds.put(firstWindow, firstWindow.getBounds());
            return;
        }

        if (!wasWindowMoved(firstWindow, previousBounds.get(firstWindow))) {
            return;
        }

        int dx = firstWindow.getX() - previousBounds.get(firstWindow).x;
        int dy = firstWindow.getY() - previousBounds.get(firstWindow).y;


        for (Window window : windows) {
            if (window.equals(firstWindow)) {
                continue;
            }

            window.setLocation(window.getX() + dx, window.getY() + dy);
            previousBounds.put(window, window.getBounds());
        }

        previousBounds.put(firstWindow, firstWindow.getBounds());
    }

    private boolean wasWindowMoved(Component firstWindow, Rectangle prevBounds) {
        if (firstWindow.getBounds().equals(prevBounds)) {
            return false;
        }
        return !((Math.abs(firstWindow.getY() - prevBounds.getY()) ==
                (int) Math.abs(firstWindow.getHeight() - prevBounds.getHeight())
                || Math.abs(firstWindow.getY() - prevBounds.getY()) == 0)
                && (Math.abs(firstWindow.getX() - prevBounds.getX()) ==
                Math.abs(firstWindow.getWidth() - prevBounds.getWidth())
                || Math.abs(firstWindow.getX() - prevBounds.getX()) ==
                0));
    }
}
