package manager.group.handlers;

import manager.group.RemoveWindowCallback;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.util.Collection;
import java.util.Map;

/**
 * @author vlad.topala
 */
public interface EventHandler {

    public void handleEvent(ComponentEvent event, Map<Component, Rectangle> previousBounds,
                            boolean movementOn, Collection<Window> windows, RemoveWindowCallback callback);

}
