package manager.group;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import manager.shadow.Intersection;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author vlad.topala
 */
public class GroupsManager {

    private final List<GluedGroup> groups = Lists.newArrayList();
    private final Map<Window, GluedGroup> windowToGroup = Maps.newHashMap();
    private final RemoveWindowCallback removeWindowCallback = new RemoveWindowCallback() {
        @Override
        public void onRemoveWindow(Window w) {
            removeWindowFromGroup(w);
        }
    };

    public void groupWindows(Window source, Set<Intersection> intersections) {

        GroupMagnet.moveWindowToGroup(source, intersections);

        if (areAllIntersectionsInSameGroup(source, intersections)) {
            return;
        }

        Set<Window> intersectedWindows = getIntersectedWindows(intersections);
        Set<GluedGroup> intersectedGroups = getIntersectionGroups(intersectedWindows);

        if (intersectedGroups.isEmpty()) {
            intersectedWindows.add(source);
            GluedGroup gluedGroup = new GluedGroup(removeWindowCallback, intersectedWindows);
            groups.add(gluedGroup);
            for (Window intersectedWindow : intersectedWindows) {
                windowToGroup.put(intersectedWindow, gluedGroup);
            }
        } else if (intersectedGroups.size() == 1) {
            GluedGroup group = intersectedGroups.iterator().next();
            group.addWindow(source);
            windowToGroup.put(source, group);
        } else {
            mergeGroups(source, intersectedGroups);
        }
    }

    private void mergeGroups(Window merger, Set<GluedGroup> intersectedGroups) {
        GluedGroup resultingGroup = new GluedGroup(removeWindowCallback, merger);
        for (GluedGroup intersectedGroup : intersectedGroups) {
            Set<Window> windows = intersectedGroup.getWindows();

            resultingGroup.addWindows(windows);
            windowToGroup.keySet().removeAll(windows);
            groups.remove(intersectedGroup);

            intersectedGroup.cleanUp();
        }
        for (Window window : resultingGroup.getWindows()) {
            windowToGroup.put(window, resultingGroup);
        }
        groups.add(resultingGroup);
    }

    /**
     * When removing a window from a group we take in consideration the cases:
     * - only 2 windows in group; if one is removed => no group remaining
     * - more windows in group but w is at the edge of the group => group remains
     * - more windows and w is in the middle => removing w creates multiple smaller groups
     *
     * @param w - window to be removed
     */
    public void removeWindowFromGroup(Window w) {
        if (!windowToGroup.containsKey(w)) {
            return;
        }

        GluedGroup group = windowToGroup.remove(w);
        Collection<GluedGroup> resultingGroups = group.removeWindow(w);
        if (resultingGroups.isEmpty()) {
            for (Window window : group.getWindows()) {
                windowToGroup.remove(window);
            }
            groups.remove(group);
        } else if (resultingGroups.size() != 1) {
            groups.remove(group);
            group.cleanUp();
            for (GluedGroup resultingGroup : resultingGroups) {
                for (Window window : resultingGroup.getWindows()) {
                    windowToGroup.put(window, resultingGroup);
                }
                groups.add(resultingGroup);
            }
        }
    }

    private Set<Window> getIntersectedWindows(Set<Intersection> intersections) {

        Set<Window> intersectedWindows = Sets.newHashSet();

        for (Intersection intersection : intersections) {
            intersectedWindows.add(intersection.getWindow());
        }

        return intersectedWindows;

    }

    private Set<GluedGroup> getIntersectionGroups(Set<Window> intersectedWindows) {

        Set<GluedGroup> intersectedGroups = Sets.newHashSet();

        for (Window intersection : intersectedWindows) {
            intersectedGroups.add(windowToGroup.get(intersection));
        }

        intersectedGroups.remove(null);

        return intersectedGroups;

    }

    public void setMovementOn(boolean movementOn) {
        for (GluedGroup group : groups) {
            group.setMovementOn(movementOn);
        }
    }

    public boolean areAllIntersectionsInSameGroup(Window source, Set<Intersection> intersections) {
        boolean sameGroup = !intersections.isEmpty() && windowToGroup.containsKey(source);

        for (Intersection intersection : intersections) {
            if (!sameGroup) {
                return false;
            }
            sameGroup = Objects.equals(windowToGroup.get(source), windowToGroup.get(intersection.getWindow()));
        }

        return sameGroup;
    }
}
