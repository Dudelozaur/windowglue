package manager.group;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import manager.SnapChecker;
import manager.shadow.Intersection;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author vlad.topala
 */
public class GluedGroup {

    private final Set<Window> windows;
    private final RemoveWindowCallback callback;
    private final GroupComponentListener componentListener;
    private final GroupFocusListener focusListener;

    public GluedGroup(RemoveWindowCallback callback, Window... windows) {
        this(callback, Sets.newHashSet(windows));
    }

    public GluedGroup(RemoveWindowCallback callback, Collection<Window> windows) {
        this.callback = callback;
        this.windows = Sets.newHashSet(windows);
        componentListener = new GroupComponentListener(this.windows, callback);
        focusListener = new GroupFocusListener(this.windows);
    }

    public boolean isWindowInGroup(Window w) {
        return windows.contains(w);
    }

    public void addWindows(Collection<Window> newWindows) {
        for (Window window : newWindows) {
            addWindow(window);
        }
    }

    public void addWindow(Window w) {
        windows.add(w);
        componentListener.addWindow(w);
        focusListener.addWindow(w);
    }

    public Collection<GluedGroup> removeWindow(Window w) {
        windows.remove(w);
        componentListener.removeWindow(w);
        focusListener.removeWindow(w);

        if (windows.size() == 1) {
            componentListener.removeWindows(windows);
            focusListener.removeWindows(windows);
            return Sets.newHashSet();
        } else {
            return Sets.newHashSet(getResultingGroups());
        }
    }

    private Collection<GluedGroup> getResultingGroups() {

        Map<Window, Collection<Window>> windowToNeighbours = getInitialNeighbours();

        for (Window window : windows) {
            populateNeighbours(window, windowToNeighbours, Sets.newHashSet(window));
            windowToNeighbours.get(window).remove(window);
        }

        return extractGroupsFromAvailablePaths(windowToNeighbours);
    }

    private Collection<GluedGroup> extractGroupsFromAvailablePaths(Map<Window, Collection<Window>> windowToNeighbours) {
        List<Window> windows = Lists.newArrayList(this.windows);
        List<GluedGroup> newGroups = Lists.newArrayList();

        while (!windows.isEmpty()) {
            Window currentWindow = windows.get(0);
            Collection<Window> neighbours = windowToNeighbours.get(currentWindow);
            neighbours.add(currentWindow);
            GluedGroup newGroup = new GluedGroup(callback, neighbours);
            windows.removeAll(neighbours);
            newGroups.add(newGroup);
        }
        return newGroups;
    }

    private Map<Window, Collection<Window>> getInitialNeighbours() {
        SnapChecker snapChecker = getSnapChecker();

        Map<Window, Collection<Window>> windowToNeighbours = Maps.newHashMap();
        for (Window window : windows) {
            HashSet<Window> neighbours = Sets.newHashSet();
            for (Intersection intersection : snapChecker.getAllIntersections(window)) {
                if (windows.contains(intersection.getWindow())) {
                    neighbours.add(intersection.getWindow());
                }
            }
            windowToNeighbours.put(window, neighbours);
        }
        return windowToNeighbours;
    }

    private SnapChecker getSnapChecker() {
        SnapChecker snapChecker = new SnapChecker(1);

        for (Window window : windows) {
            snapChecker.maintainShadow(window);
        }

        return snapChecker;
    }

    private void populateNeighbours(Window window, Map<Window, Collection<Window>> windows, HashSet<Window> visited) {

        for (Window neighbour : Sets.newHashSet(windows.get(window))) {
            if (!visited.contains(neighbour)) {
                visited.add(neighbour);
                populateNeighbours(neighbour, windows, visited);
                windows.get(window).addAll(windows.get(neighbour));
            }
        }

    }

    public void cleanUp() {
        componentListener.cleanUp();
        focusListener.cleanUp();
        windows.clear();
    }

    public void setMovementOn(boolean movementOn) {
        componentListener.setMovementOn(movementOn);
    }

    public Set<Window> getWindows() {
        return Sets.newHashSet(windows);
    }
}
