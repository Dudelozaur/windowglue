package manager.group;

import com.beust.jcommander.internal.Maps;
import com.beust.jcommander.internal.Sets;
import manager.shadow.IntersectionPosition;

import java.awt.*;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class NeighbourList {
    Map<IntersectionPosition, Set<Window>> neighbours = Maps.newHashMap();

    public NeighbourList() {
        neighbours.put(IntersectionPosition.LEFT, Sets.<Window>newHashSet());
        neighbours.put(IntersectionPosition.RIGHT, Sets.<Window>newHashSet());
        neighbours.put(IntersectionPosition.TOP, Sets.<Window>newHashSet());
        neighbours.put(IntersectionPosition.BOTTOM, Sets.<Window>newHashSet());
    }

    public void addNeighbour(IntersectionPosition position, Window neighbour) {
        neighbours.get(position).add(neighbour);
    }

    public void addNeighbours(IntersectionPosition position, Collection<Window> newNeighbours) {
        neighbours.get(position).addAll(newNeighbours);
    }

    public Set<Window> getNeighbours(IntersectionPosition position) {
        return neighbours.get(position);
    }
}
