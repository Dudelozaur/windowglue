package matching;

import java.awt.*;

/**
 * @author vlad.topala
 */
public interface Matcher {

    public boolean matches(Window w);

}
