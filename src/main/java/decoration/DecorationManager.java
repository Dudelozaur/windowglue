package decoration;

import com.google.common.collect.Lists;
import manager.shadow.Intersection;

import java.util.List;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class DecorationManager {
    private final List<WindowDecorator> decorators = Lists.newArrayList();

    public void decorateWindows(Set<Intersection> possibleIntersections) {
        for (WindowDecorator decorator : decorators) {
            for (Intersection possibleIntersection : possibleIntersections) {
                decorator.decorateWindow(possibleIntersection.getWindow());
            }
        }
    }

    public void clearDecorations() {
        for (WindowDecorator decorator : decorators) {
            decorator.undecorateAllWindows();
        }
    }

    public void addWindowDecorator(WindowDecorator decorator) {
        decorators.add(decorator);
    }

    public void removeWindowDecorator(WindowDecorator decorator) {
        decorators.remove(decorator);
    }
}
