package decoration;

import com.google.common.collect.Maps;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * @author vlad.topala
 */
public class TransparentOverViewDecorator implements WindowDecorator {

    private Map<Window, Window> windowToDecorator = Maps.newHashMap();

    @Override
    public void decorateWindow(Window w) {

        if (windowToDecorator.containsKey(w)) {
            return;
        }

        JWindow transparent = new JWindow();
        windowToDecorator.put(w, transparent);

        transparent.setOpacity((float) .7);
        transparent.setBounds(w.getBounds());
        transparent.setAlwaysOnTop(true);

        JPanel p = new JPanel();
        p.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 5));
        transparent.add(p);
        transparent.setName("q");
        transparent.setVisible(true);

    }

    @Override
    public void undecorateWindow(Window w) {
        if (windowToDecorator.containsKey(w)) {
            windowToDecorator.remove(w).dispose();
        }
    }

    @Override
    public void undecorateAllWindows() {
        for (Map.Entry<Window, Window> windowWindowEntry : windowToDecorator.entrySet()) {
            windowWindowEntry.getValue().dispose();
        }
        windowToDecorator.clear();
    }
}
