package decoration;

import java.awt.*;

/**
 * @author vlad.topala
 */
public interface WindowDecorator {

    public void decorateWindow(Window w);

    public void undecorateWindow(Window w);

    public void undecorateAllWindows();

}
