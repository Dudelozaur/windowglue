import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author vlad.topala
 */
public class FPSDemo {

    private int frames;

    public FPSDemo() {
        RepaintManager.setCurrentManager(new RepaintManager() {
            @Override
            public void paintDirtyRegions() {
                super.paintDirtyRegions();
                frames++;
            }
        });

        Timer t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(frames);
                frames = 0;
            }
        });
        t.start();
    }
}
