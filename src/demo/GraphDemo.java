import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

/**
 * @author vlad.topala
 */
public class GraphDemo {
    public GraphDemo() {
        UndirectedSparseMultigraph<Window, Number> g = new UndirectedSparseMultigraph<>();
        g.addVertex(new JFrame("1"));
        JFrame w2 = new JFrame("2");
        g.addVertex(w2);
        JFrame w3 = new JFrame("3");
        g.addVertex(w3);
        g.addEdge(g.getEdgeCount(), w2, w3);

        WeakComponentClusterer<Window, Number> clusterer = new WeakComponentClusterer<>();

        Set<Set<Window>> transform = clusterer.transform(g);
    }

    public static void main(String[] args) {
        new GraphDemo();
    }
}
