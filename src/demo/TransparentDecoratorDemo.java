import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * @author vlad.topala
 */
public class TransparentDecoratorDemo {

    public static void main(String[] args) {
        JPanel pnlBorder = new JPanel();
        pnlBorder.setBorder(new LineBorder(Color.ORANGE, 5));

        JWindow w = new JWindow();
        w.add(pnlBorder);
        w.setOpacity((float) 0.5);
        w.setVisible(true);
        w.setAlwaysOnTop(true);
        w.setSize(500, 500);
    }

}
