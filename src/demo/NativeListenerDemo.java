import manager.listeners.NativeListener;
import manager.listeners.SnapModeChangeCallback;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

/**
 * @author vlad.topala
 */
public class NativeListenerDemo {
    NativeListener n = new NativeListener(12, new SnapModeChangeCallback() {
        @Override
        public void onSnapModeChange(boolean newValue) {
        }
    });

    public NativeListenerDemo() {
        new GroupMovementDemo();
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }

        //Construct the example object and initialze native hook.
        GlobalScreen.getInstance().addNativeKeyListener(n);
        GlobalScreen.getInstance().addNativeMouseListener(n);

    }

    public static void main(String[] args) {
        new NativeListenerDemo();
    }
}
