import decoration.TransparentOverViewDecorator;
import manager.GlueManager;
import manager.listeners.ListenerType;
import matching.Matcher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * @author vlad.topala
 */
public class MovementDemo {

    static boolean nativeTitleBar = true;

    public MovementDemo() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    JFrame w = new JFrame("Window" + i);
                    w.setName("w" + i);
                    JButton b = new JButton("B" + i);
                    if (!nativeTitleBar) {
                        w.setUndecorated(true);
                        w.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
                    }
                    w.add(b);
                    w.setVisible(true);
                    w.setSize(200, 200);
                }
            }
        });
    }

    public static void main(String[] args) {
        GlueManager m = new GlueManager(20, nativeTitleBar ? KeyEvent.CTRL_MASK : KeyEvent.CTRL_DOWN_MASK
                , nativeTitleBar ? ListenerType.NATIVE : ListenerType.JAVA);
        m.addWindowDecorator(new TransparentOverViewDecorator());
        m.addWindowMatcher(new Matcher() {
            @Override
            public boolean matches(Window w) {
                return !"q".equals(w.getName());
            }
        });
        new MovementDemo();
//        new FPSDemo();
    }

}
