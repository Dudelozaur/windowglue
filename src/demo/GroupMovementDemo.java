import decoration.TransparentOverViewDecorator;
import manager.GlueManager;
import manager.group.GluedGroup;
import manager.group.RemoveWindowCallback;
import matching.Matcher;

import javax.swing.*;
import java.awt.*;

/**
 * @author vlad.topala
 */
public class GroupMovementDemo {

    private final GluedGroup gluedGroup = new GluedGroup(new RemoveWindowCallback() {
        @Override
        public void onRemoveWindow(Window w) {
        }
    });

    public GroupMovementDemo() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame w1 = new JFrame("Window1");
                JFrame w2 = new JFrame("Window2");
                JFrame w3 = new JFrame("Window3");

                w1.setName("w1");
                w2.setName("w2");
                w3.setName("w3");

                JButton b1 = new JButton("B1");
                JButton b2 = new JButton("B2");
                JButton b3 = new JButton("B3");

                boolean nativeTitleBar = true;
                if (!nativeTitleBar) {
                    w1.setUndecorated(true);
                    w1.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
                    w2.setUndecorated(true);
                    w2.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
                    w3.setUndecorated(true);
                    w3.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
                }

                w1.add(b1);
                w2.add(b2);
                w3.add(b3);

                w1.setVisible(true);
                w2.setVisible(true);
                w3.setVisible(true);

                w1.setSize(200, 200);
                w2.setSize(200, 200);
                w3.setSize(400, 200);

                w2.setLocation(w1.getX() + w1.getWidth(), w2.getY());
                w3.setLocation(w1.getX(), w2.getY() + w2.getHeight());

                w1.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                w2.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                w3.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                gluedGroup.addWindow(w1);
                gluedGroup.addWindow(w2);
                gluedGroup.addWindow(w3);

            }
        });

    }

    public static void main(String[] args) {

        new GroupMovementDemo();
        GlueManager m = new GlueManager();
        m.addWindowDecorator(new TransparentOverViewDecorator());
        m.addWindowMatcher(new Matcher() {
            @Override
            public boolean matches(Window w) {
                return !"q".equals(w.getName());
            }
        });
    }
}
